﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

namespace GrussPRandomFileLauncher
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            var rng = new Random();

            try
            {
                var searchOption = Properties.Settings.Default.Recurse ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly;
                var allowedExtensions = Properties.Settings.Default.AllowedExtensions?.Split(',').Select(s => s.ToUpper()) ?? new string[] { };
                string[] omitPaths;
                if ((Properties.Settings.Default.ExcludePaths?.Count ?? 0) == 0)
                {
                    omitPaths = new string[] { };
                }
                else
                {
                    omitPaths = new string[Properties.Settings.Default.ExcludePaths.Count];
                    Properties.Settings.Default.ExcludePaths.CopyTo(omitPaths, 0);
                }
                var fsi = Directory.EnumerateFiles(Properties.Settings.Default.ScanPath, "*.*", searchOption).Where(s => MatchPath(s, allowedExtensions, omitPaths)).ToArray();

                if (fsi.Length == 0) throw new InvalidOperationException("No matching files were found to launch.");

                var selectedPath = fsi[rng.Next(fsi.Length)];

                var psi = new ProcessStartInfo(selectedPath) { UseShellExecute = true, WorkingDirectory = Properties.Settings.Default.ScanPath };
                new Process { StartInfo = psi }.Start();
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Running from {Environment.CurrentDirectory}:\r\n{ex}", "Gruss_P Random File Launcher Error");
            }
        }

        private static bool MatchPath(string candidatePath, IEnumerable<string> allowedExtensions, IEnumerable<string> omitPathPrefixes)
        {
            var candidateExtension = Path.GetExtension(candidatePath).ToUpper().PadRight(1).Substring(1);
            if (!allowedExtensions.Contains(candidateExtension)) return false;
            return omitPathPrefixes.All(omit => !candidatePath.StartsWith(omit, StringComparison.InvariantCultureIgnoreCase));
        }
    }
}
